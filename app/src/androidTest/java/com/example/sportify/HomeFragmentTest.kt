package com.example.sportify

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.sportify.presentation.home.HomeFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.example.sportify.R
import androidx.test.espresso.assertion.ViewAssertions.matches

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class HomeFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun testHomeFragmentIsDisplayed() {
        launchFragmentInHiltContainer<HomeFragment>()
        onView(withId(R.id.home_fragment)).check(matches(isDisplayed()))
    }
}
