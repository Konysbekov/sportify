package com.example.sportify.presentation.home

import PopularOrganizersAdapter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.sportify.data.Service
import com.example.sportify.databinding.FragmentHomeBinding
import com.example.sportify.entity.SportEvent
import com.example.sportify.entity.User
import com.example.sportify.presentation.EventDetailsBottomSheet
import com.example.sportify.presentation.home.adapter.PopularEventViewHolder
import com.example.sportify.presentation.home.adapter.PopularEventAdapter
import com.example.sportify.presentation.home.adapter.PopularOrganizers
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by viewModels()
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private var dataList = mutableListOf<PopularOrganizers>()
    private lateinit var adapterOrganizers: PopularOrganizersAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {}
        homeViewModel.events.observe(viewLifecycleOwner) { eventList ->
            // Update RecyclerView adapter with new event list
        }
        homeViewModel.loadEvents()
        adapterOrganizers = PopularOrganizersAdapter(dataList)
        binding.popularOrganizersRv.adapter = adapterOrganizers
        loadPopularEvents()
        loadMyEvents()
    }

    private fun updateData(newDataList: List<PopularOrganizers>) {
        dataList.clear()
        dataList.addAll(newDataList)
        adapterOrganizers.notifyDataSetChanged()
    }

    private fun loadPopularEvents() {
        val query = Service.getEventsDataRef().orderByChild("participantsNumber").limitToLast(4)
        val popularEvents = FirebaseRecyclerOptions.Builder<SportEvent>().setQuery(query, SportEvent::class.java).build()
        val popularEventsAdapter = PopularEventAdapter(popularEvents) {
            val bottomSheetFragment = EventDetailsBottomSheet.newInstance(it)
            bottomSheetFragment.show(childFragmentManager, bottomSheetFragment.tag)
        }
        popularEventsAdapter.startListening()
        binding.popularEventsRv.adapter = popularEventsAdapter
    }

    private fun loadMyEvents() {
        val query = Service.getEventsDataRef().orderByChild("participants/${Service.getCurrentUser()?.uid}").equalTo(true)
        val myEvents = FirebaseRecyclerOptions.Builder<SportEvent>().setQuery(query, SportEvent::class.java).build()
        val myEventsAdapter = PopularEventAdapter(myEvents) {
            val bottomSheetFragment = EventDetailsBottomSheet.newInstance(it)
            bottomSheetFragment.show(childFragmentManager, bottomSheetFragment.tag)
        }
        myEventsAdapter.startListening()
        binding.myEvents.adapter = myEventsAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun mapUsersToPopularOrganizers(users: List<User>): List<PopularOrganizers> {
        val popularOrganizersList = mutableListOf<PopularOrganizers>()
        for (user in users) {
            val organizer = PopularOrganizers(
                organizerName = user.fullName,
                organizerStatus = user.userName,
                category = "Default Category",
                photo = user.photo,
            )
            popularOrganizersList.add(organizer)
        }
        return popularOrganizersList
    }
}
