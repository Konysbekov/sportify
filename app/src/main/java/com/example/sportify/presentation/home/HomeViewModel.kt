package com.example.sportify.presentation.home

import androidx.lifecycle.*
import com.example.sportify.entity.SportEvent
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlinx.coroutines.channels.awaitClose


@HiltViewModel
class HomeViewModel @Inject constructor(
    private val database: DatabaseReference
) : ViewModel() {

    private val _events = MutableLiveData<List<SportEvent>>()
    val events: LiveData<List<SportEvent>> get() = _events

    fun loadEvents() {
        viewModelScope.launch {
            database.child("events").getEventsFlow()
                .flowOn(Dispatchers.IO)
                .collect { eventList ->
                    _events.postValue(eventList)
                }
        }
    }
}

fun DatabaseReference.getEventsFlow(): Flow<List<SportEvent>> = callbackFlow {
    val listener = object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
            val eventList = snapshot.children.mapNotNull { it.getValue(SportEvent::class.java) }
            trySend(eventList).isSuccess
        }

        override fun onCancelled(error: DatabaseError) {
            close(error.toException())
        }
    }
    addValueEventListener(listener)
    awaitClose { removeEventListener(listener) }
}
